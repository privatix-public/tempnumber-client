# Num SDK

TypeScript SDK  for temp-number.org/business mass phone activation service

# Install

Install the package using npm

```bash
npm install tempnumber-client
```

# Usage/Examples

```typescript
import NumSdk, * as sdk from 'tempnumber-client'

const api = new NumSdk({ apiKey: 'YOUR_API_KEY', baseUrl: 'https://business.temp-number.org/v1' })(async () => {
  const balance = await this.api.user.getBalance()
  console.log(`My balance is ${balance.total}`)
})()

/* Getting a number to activate Twitter service in US */
api.activation
  .newActivation({ service_id: 527, country_id: 'us' })
  .then(async (activation: NumSdk.newActivationResponseType) => {
    // Send message to number activation.phone

    // Receive sms
    const activationStatus = await api.activation.getActivationState(activation.id)
    // Use received sms message and OTP code parsed from it
    // activationStatus.sms_code is a parsed activation code from sms message
    // activationStatus.sms_text is a full sms message
  })
```
