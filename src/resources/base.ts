import axios, { AxiosInstance, AxiosResponse } from 'axios'
import {
  UnauthorizedException,
  TooManyRequestsException,
  InvalidRequestParamsException,
  ResourceNotFoundException,
  ResourceBadStateException,
  ServiceUnavailableException,
  InternalException
} from './exceptions'

type Config = {
  apiKey: string
  baseUrl?: string
}

export abstract class Base {
  private apiKey: string
  private baseUrl: string
  private axios: AxiosInstance

  constructor(config: Config) {
    this.apiKey = config.apiKey
    this.baseUrl = config.baseUrl || 'https://business.temp-number.org/api/v1'
    this.axios = axios.create({
      baseURL: this.baseUrl,
      headers: {
        'X-API-Key': this.apiKey,
        'content-type': 'application/json'
      }
    })
  }

  public setCustomHeader(key: string, value: string) {
    this.axios.defaults.headers[key] = value
  }

  public removeCustomHeader(key: string) {
    if (this.axios.defaults.headers[key]) delete this.axios.defaults.headers[key]
  }

  protected async apiCall(enpoint: string, data?: object, method = 'get'): Promise<AxiosResponse> {
    let response
    try {
      response = await this.axios.request({
        url: enpoint,
        method: method,
        data: method !== 'get' ? data : undefined,
        params: method === 'get' ? data : undefined
      })
    } catch (err) {
      if (err.isAxiosError === true && err.response && err.response.status) {
        switch (err.response.status) {
          case 401:
            throw new UnauthorizedException(err)
          case 429:
            const exc = new TooManyRequestsException(err)
            exc.setHeader(err.response.headers)
            throw exc
          case 400:
            throw new InvalidRequestParamsException(err)
          case 404:
            throw new ResourceNotFoundException(err)
          case 409:
            throw new ResourceBadStateException(err)
          case 503:
            throw new ServiceUnavailableException(err)
          default:
            throw new InternalException(err)
        }
      } else {
        throw new InternalException(err)
      }
    }
    return response
  }
}
