import { Base } from '../base'
import { StripePaymentIntentType, osType, PaymentStatusResponseType } from '../../types/payments'

export class Payments extends Base {
  public async createStripePayment(amount: number, os: osType, userAgent: string): Promise<StripePaymentIntentType> {
    const response = await this.apiCall(
      '/payments/stripe',
      {
        amount,
        os,
        userAgent
      },
      'post'
    )
    return response.data
  }

  public async getStripePurchaseStatus(id: string): Promise<PaymentStatusResponseType> {
    const response = await this.apiCall('/payments/stripe', {
      id
    })
    return response.data
  }
}
