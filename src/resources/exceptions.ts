export class Exception extends Error {
  public name: string
  public message: string
  public status: number
  public parentError: object | undefined

  constructor(parentError: object | undefined, message: string, status: number = 500) {
    super(message)
    /**
     * Set error message
     */
    Object.defineProperty(this, 'parentError', {
      configurable: true,
      enumerable: false,
      value: parentError,
      writable: true
    })

    /**
     * Set error message
     */
    Object.defineProperty(this, 'message', {
      configurable: true,
      enumerable: false,
      value: message,
      writable: true
    })

    /**
     * Set error name as a public property
     */
    Object.defineProperty(this, 'name', {
      configurable: true,
      enumerable: false,
      value: this.constructor.name,
      writable: true
    })

    /**
     * Set status as a public property
     */
    Object.defineProperty(this, 'status', {
      configurable: true,
      enumerable: false,
      value: status,
      writable: true
    })

    /**
     * Update the stack trace
     */
    Error.captureStackTrace(this, this.constructor)
  }
}

export class UnauthorizedException extends Exception {
  constructor(parentError?: object | undefined, message?: string, status?: number) {
    message = message || 'Unauthorized to access this resource'
    status = status || 401
    super(parentError, message, status)
  }
}

export class TooManyRequestsException extends Exception {
  public xRateLimitLimit: number | null = null // Maximum allowed number of requests.
  public xRateLimitRemaining: number | null = null // Remaining allowed number of requests.
  public xRateLimitReset: number | null = null //Unix timestamp when the rate limit will reset.
  public retryAfter: number | null = null // Number of seconds to wait before retrying the request.
  constructor(parentError?: object | undefined, message?: string, status?: number) {
    message = message || 'Too Many Requests'
    status = status || 429
    super(parentError, message, status)
  }

  setHeader(response: Array<string> | { [key: string]: string }) {
    if (response['x-ratelimit-limit']) {
      this.xRateLimitLimit = parseInt(response['x-ratelimit-limit'])
    }
    if (response['x-ratelimit-remaining']) {
      this.xRateLimitRemaining = parseInt(response['x-ratelimit-remaining'])
    }
    if (response['x-ratelimit-reset']) {
      this.xRateLimitReset = parseInt(response['x-ratelimit-reset'])
    }
    if (response['retry-after']) {
      this.retryAfter = parseInt(response['retry-after'])
    }
  }
}

export class InvalidRequestParamsException extends Exception {
  constructor(parentError?: object | undefined, message?: string, status?: number) {
    message = message || 'Invalid request params'
    status = status || 400
    super(parentError, message, status)
  }
}

export class ResourceNotFoundException extends Exception {
  constructor(parentError?: object | undefined, message?: string, status?: number) {
    message = message || 'Resource not found'
    status = status || 404
    super(parentError, message, status)
  }
}

export class ResourceBadStateException extends Exception {
  constructor(parentError?: object | undefined, message?: string, status?: number) {
    message = message || 'Resource is in a bad state'
    status = status || 409
    super(parentError, message, status)
  }
}

export class ServiceUnavailableException extends Exception {
  constructor(parentError?: object | undefined, message?: string, status?: number) {
    message = message || 'Service Unavailable'
    status = status || 503
    super(parentError, message, status)
  }
}

export class InternalException extends Exception {
  constructor(parentError?: object | undefined, message?: string, status?: number) {
    message = message || 'Internal Error'
    status = status || 500
    super(parentError, message, status)
  }
}
