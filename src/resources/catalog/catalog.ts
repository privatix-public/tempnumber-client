import { Base } from '../base'
import {
  CountriesType,
  ServicesType,
  PricesByCountries,
  PricesByServices,
  ServicePriceType,
  CountryPriceType,
  PricesCount
} from '../../types/catalog'

export class Catalog extends Base {
  public async getCountries(): Promise<CountriesType> {
    const response = await this.apiCall('/countries')
    return response.data
  }

  public async getServices(): Promise<ServicesType> {
    const response = await this.apiCall('/services')
    return response.data
  }

  public async getPricesByCountries(): Promise<PricesByCountries[]> {
    const response = await this.apiCall('/prices/countries')
    return response.data
  }

  public async getPricesByServices(): Promise<PricesByServices[]> {
    const response = await this.apiCall('/prices/services')
    return response.data
  }

  public async getPricesInOneCountry(countryId: string): Promise<ServicePriceType[]> {
    const response = await this.apiCall(`/prices/countries/${countryId}/services`)
    return response.data
  }

  public async getPricesForOneService(serviceId: number): Promise<CountryPriceType[]> {
    const response = await this.apiCall(`/prices/services/${serviceId}/countries`)
    return response.data
  }

  public async getPricesForServiceInOneCountry(serviceId: number, countryId: string): Promise<PricesCount> {
    const response = await this.apiCall(`/prices/services/${serviceId}/countries/${countryId}`)
    return response.data
  }
}
