import { Base } from '../base'
import {
  newActivationResponseType,
  newActivationRequestType,
  ActivationStatusType,
  ActivationsHistoryType,
  successResponseType,
  NumActivationStatus
} from '../../types/activation'
import { InternalException } from '../exceptions'

export class Activation extends Base {
  public async newActivation(payload: newActivationRequestType): Promise<newActivationResponseType> {
    const response = await this.apiCall('/activation', payload, 'post')
    return response.data
  }

  public async getActivationState(id: string): Promise<ActivationStatusType> {
    const response = await this.apiCall(`/activation/${id}`)
    try {
      response.data.status = NumActivationStatus[response.data.status]
    } catch (err) {
      throw new InternalException(err, 'Invalid activation status')
    }
    return response.data
  }

  public async getActivationsHistory(page: number = 1, limit: number = 10): Promise<ActivationsHistoryType> {
    const response = await this.apiCall(`/activations`, { page, limit })
    try {
      for (let i = 0; i < response.data.activations.length; i++) {
        response.data.activations[i].status = NumActivationStatus[response.data.activations[i].status]
      }
    } catch (err) {
      throw new InternalException(err, 'Invalid activation status')
    }
    return response.data
  }

  public async retryActivation(id: string): Promise<successResponseType> {
    const response = await this.apiCall(`/activation/${id}/retry`, undefined, 'put')
    return response.data
  }

  public async cancelActivation(id: string): Promise<successResponseType> {
    const response = await this.apiCall(`/activation/${id}/cancel`, undefined, 'put')
    return response.data
  }
}
