import { Base } from '../base'
import { UserBalanceType } from '../../types/user'

export class User extends Base {
  public async getBalance(): Promise<UserBalanceType> {
    const response = await this.apiCall('/user/balance')
    return response.data
  }

  public async getApiKey(): Promise<string> {
    const response = await this.apiCall('/user/apikey')
    if (!response.data.key) {
      throw new Error('No API key found')
    }
    return response.data.key
  }

  public async changeApiKey(): Promise<string> {
    const response = await this.apiCall('/user/apikey', undefined, 'put')
    if (!response.data.key) {
      throw new Error('No API key found')
    }
    return response.data.key
  }
}
