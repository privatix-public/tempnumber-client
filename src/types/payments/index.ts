export declare type StripePaymentIntentType = {
  id: string
  clientSecret: string
  publicKey: string
}

export enum osType {
  'ios' = 'ios',
  'android' = 'android',
  'web' = 'web'
}

export enum paymentStatus {
  'paid' = 'paid',
  'review' = 'review',
  'refunded' = 'refunded'
}

export declare type PaymentStatusResponseType = {
  status: paymentStatus
}
