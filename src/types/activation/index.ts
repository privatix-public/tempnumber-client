export declare type newActivationResponseType = {
  id: string
  phone: number
}

export declare type newActivationRequestType = {
  service_id: number
  country_id: string
  max_price?: number
}

export enum ActivationStatus {
  smsRequested = 'smsRequested',
  smsReceived = 'smsReceived',
  retryRequested = 'retryRequested',
  retryReceived = 'retryReceived',
  canceled = 'canceled',
  error = 'error'
}

export const NumActivationStatus = {
  2: ActivationStatus.smsRequested,
  3: ActivationStatus.smsReceived,
  4: ActivationStatus.retryRequested,
  5: ActivationStatus.retryReceived,
  6: ActivationStatus.canceled,
  7: ActivationStatus.error
}

export declare type ActivationStatusType = {
  id: string
  phone: number
  country_id: string
  service_id: number
  status: ActivationStatus
  sms_code: string | null
  sms_text: string | null
  expire_at: number
  updated_at: number
}

export declare type ActivationsHistoryType = {
  activations: Array<ActivationStatusType>
  page: number
  limit: number
  pages: number
  total: number
}

export declare type successResponseType = {
  result: string
}
