export declare type UserBalanceType = {
  total: number
  reserved: number
}
