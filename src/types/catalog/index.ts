export declare type CountryType = {
  id: string
  name: string
}

export declare type CountriesType = Array<CountryType>

export declare type ServiceType = {
  id: number
  name: string
}
export declare type ServicesType = Array<ServiceType>

export declare type ServicePriceType = {
  id: number
  price: number
  count: number
}

export declare type PricesByCountries = {
  id: string
  services: ServicePriceType[]
}

export declare type CountryPriceType = {
  id: string
  price: number
  count: number
}

export declare type PricesByServices = {
  id: number
  countries: CountryPriceType[]
}

export declare type PricesCount = {
  price: number
  count: number
}
