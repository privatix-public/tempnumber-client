import { Activation } from './resources/activation/activation'
import { User } from './resources/user/user'
import { Catalog } from './resources/catalog/catalog'
import { Payments } from './resources/payments/payments'

export * from './types'
export * from './resources/exceptions'

export default class TNumbers {
  user: User
  activation: Activation
  catalog: Catalog
  payments: Payments

  constructor(config: { apiKey: string; baseUrl?: string }) {
    this.user = new User(config)
    this.activation = new Activation(config)
    this.catalog = new Catalog(config)
    this.payments = new Payments(config)
  }
}
