import { test } from '@japa/runner'
import TNumbers from '../src/index'
import { StripePaymentIntentType, osType, PaymentStatusResponseType, paymentStatus } from '../src/types/payments'

const sdk = new TNumbers({ apiKey: 'test', baseUrl: 'http://localhost:3000/business/api/v1' })
test('Create stripe payment intent', async ({ assert }) => {
  const PaymentIntentData: StripePaymentIntentType = {
    id: 'pi_3MgoJsIPMzVE2',
    clientSecret: 'pi_3MgoJsIPMzVE2_secret_Dy55',
    publicKey: 'pk_test_51Meel8IPMzVEnbiR'
  }

  const res = await sdk.payments.createStripePayment(
    5,
    osType.ios,
    'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148'
  )
  assert.deepEqual(res, PaymentIntentData)
})

test('Get stripe purchase status', async ({ assert }) => {
  const PaymentIntentData: PaymentStatusResponseType = {
    status: paymentStatus.review
  }

  const res = await sdk.payments.getStripePurchaseStatus('pi_3MsmzuINPemAv2eo0vBuxg3H')
  assert.deepEqual(res, PaymentIntentData)
})
