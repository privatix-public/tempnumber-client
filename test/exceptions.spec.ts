import { test } from '@japa/runner'
import TNumbers from '../src/index'
import {
  UnauthorizedException,
  TooManyRequestsException,
  InvalidRequestParamsException,
  ResourceNotFoundException,
  ResourceBadStateException,
  ServiceUnavailableException
} from '../src/resources/exceptions'

import { osType } from '../src/types/payments'

const sdk = new TNumbers({ apiKey: 'test', baseUrl: 'http://localhost:3000/business/api/v1' })

test('401: UnauthorizedException', async ({ assert }) => {
  sdk.user.setCustomHeader('Prefer', 'statusCode=401')
  try {
    await sdk.user.getBalance()
    // SDK must return an exception, otherwise throw an error
    throw new Error()
  } catch (err) {
    assert.instanceOf(err, UnauthorizedException)
  }
})

test('429: TooManyRequestsException', async ({ assert }) => {
  sdk.user.setCustomHeader('Prefer', 'statusCode=429')
  try {
    await sdk.user.getBalance()
    // SDK must return an exception, otherwise throw an error
    throw new Error()
  } catch (err) {
    assert.instanceOf(err, TooManyRequestsException)
  }
})

test('400: InvalidRequestParamsException', async ({ assert }) => {
  sdk.activation.setCustomHeader('Prefer', 'statusCode=400')
  try {
    await sdk.activation.newActivation({ service_id: 1, country_id: 'us', max_price: 1 })
    // SDK must return an exception, otherwise throw an error
    throw new Error()
  } catch (err) {
    assert.instanceOf(err, InvalidRequestParamsException)
  }
})

test('404: ResourceNotFoundException', async ({ assert }) => {
  sdk.activation.setCustomHeader('Prefer', 'statusCode=404')
  try {
    await sdk.activation.newActivation({ service_id: 1, country_id: 'us', max_price: 1 })
    // SDK must return an exception, otherwise throw an error
    throw new Error()
  } catch (err) {
    assert.instanceOf(err, ResourceNotFoundException)
  }
})

test('409: ResourceBadStateException', async ({ assert }) => {
  sdk.activation.setCustomHeader('Prefer', 'statusCode=409')
  try {
    await sdk.activation.newActivation({ service_id: 1, country_id: 'us', max_price: 1 })
    // SDK must return an exception, otherwise throw an error
    throw new Error()
  } catch (err) {
    assert.instanceOf(err, ResourceBadStateException)
  }
})

test('503: ServiceUnavailableException', async ({ assert }) => {
  sdk.payments.setCustomHeader('Prefer', 'statusCode=503')
  try {
    await sdk.payments.createStripePayment(
      5,
      osType.ios,
      'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148'
    )
    // SDK must return an exception, otherwise throw an error
    throw new Error()
  } catch (err) {
    assert.instanceOf(err, ServiceUnavailableException)
  }
})
