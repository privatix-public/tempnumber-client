import { test } from '@japa/runner'
import TNumbers from '../src/index'
import { UserBalanceType } from '../src/types/user'

const sdk = new TNumbers({ apiKey: 'test', baseUrl: 'http://localhost:3000/business/api/v1' })
test('Get user balance', async ({ assert }) => {
  const balanceData: UserBalanceType = {
    total: 2301,
    reserved: 210
  }

  const resBalance = await sdk.user.getBalance()
  assert.deepEqual(resBalance, balanceData)
})

test('Get API key', async ({ assert }) => {
  const apiKeyResponse = {
    key: 'q_EaTiX+xbBXLyO05.+zDXjI+Qi_X0v'
  }
  const resApiKey = await sdk.user.getApiKey()
  assert.equal(resApiKey, apiKeyResponse.key)
})

test('Change API key', async ({ assert }) => {
  const apiKeyResponse = {
    key: 'q_EaTiX+xbBXLyO05.+zDXjI+Qi_X0v'
  }
  const resApiKey = await sdk.user.changeApiKey()
  assert.equal(resApiKey, apiKeyResponse.key)
})
