import { test } from '@japa/runner'
import TNumbers from '../src/index'
import {
  newActivationResponseType,
  ActivationStatusType,
  ActivationsHistoryType,
  successResponseType,
  ActivationStatus
} from '../src/types/activation'

const sdk = new TNumbers({ apiKey: 'test', baseUrl: 'http://localhost:3000/business/api/v1' })
test('Create activation', async ({ assert }) => {
  const newActivationData: newActivationResponseType = {
    id: '63f4d0dab040a865d80da08a',
    phone: 447975777666
  }

  const res = await sdk.activation.newActivation({
    service_id: 1,
    country_id: 'us',
    max_price: 40
  })
  assert.deepEqual(res, newActivationData)
})

test('Get activation state', async ({ assert }) => {
  const ActivationStatusData: ActivationStatusType = {
    id: '63f4d0dab040a865d80da08a',
    phone: 447975777666,
    country_id: 'uk',
    service_id: 1,
    status: ActivationStatus.smsRequested,
    sms_code: '298423',
    sms_text: 'Your activation code is 298423',
    expire_at: 1679061628,
    updated_at: 1679061001
  }

  const res = await sdk.activation.getActivationState('63f4d0dab040a865d80da08a')
  assert.deepEqual(res, ActivationStatusData)
})

test('Get activation history', async ({ assert }) => {
  const ActivationHistoryData: ActivationsHistoryType = {
    activations: [
      {
        id: '63f4d0dab040a865d80da08a',
        phone: 447975777666,
        country_id: 'uk',
        service_id: 1,
        status: ActivationStatus.smsRequested,
        sms_code: '298423',
        sms_text: 'Your activation code is 298423',
        expire_at: 1679061628,
        updated_at: 1679061001
      }
    ],
    page: 1,
    limit: 10,
    pages: 5,
    total: 48
  }

  const res = await sdk.activation.getActivationsHistory()
  assert.deepEqual(res, ActivationHistoryData)
})

test('Retry activation', async ({ assert }) => {
  const ActivationStatusData: successResponseType = {
    result: 'success'
  }

  const res = await sdk.activation.retryActivation('63f4d0dab040a865d80da08a')
  assert.deepEqual(res, ActivationStatusData)
})

test('Cancel activation', async ({ assert }) => {
  const ActivationStatusData: successResponseType = {
    result: 'success'
  }

  const res = await sdk.activation.cancelActivation('63f4d0dab040a865d80da08a')
  assert.deepEqual(res, ActivationStatusData)
})
