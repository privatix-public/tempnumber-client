import { test } from '@japa/runner'
import TNumbers from '../src/index'
import {
  CountriesType,
  ServicesType,
  PricesByCountries,
  PricesByServices,
  ServicePriceType,
  CountryPriceType,
  PricesCount
} from '../src/types/catalog'

const sdk = new TNumbers({ apiKey: 'test', baseUrl: 'http://localhost:3000/business/api/v1' })
test('Get countries', async ({ assert }) => {
  const CountriesData: CountriesType = [
    {
      id: 'uk',
      name: 'United Kingdom'
    }
  ]

  const res = await sdk.catalog.getCountries()
  assert.deepEqual(res, CountriesData)
})

test('Get services', async ({ assert }) => {
  const ServicesData: ServicesType = [
    {
      id: 12,
      name: 'Whatsapp'
    }
  ]

  const res = await sdk.catalog.getServices()
  assert.deepEqual(res, ServicesData)
})

test('Get prices by countries', async ({ assert }) => {
  const PriceData: PricesByCountries[] = [
    {
      id: 'uk',
      services: [
        {
          id: 12,
          price: 210,
          count: 143
        }
      ]
    }
  ]

  const res = await sdk.catalog.getPricesByCountries()
  assert.deepEqual(res, PriceData)
})

test('Get prices by services', async ({ assert }) => {
  const PriceData: PricesByServices[] = [
    {
      id: 12,
      countries: [
        {
          id: 'uk',
          price: 210,
          count: 143
        }
      ]
    }
  ]

  const res = await sdk.catalog.getPricesByServices()
  assert.deepEqual(res, PriceData)
})

test('Get prices in one country', async ({ assert }) => {
  const PriceData: ServicePriceType[] = [
    {
      id: 12,
      price: 210,
      count: 143
    }
  ]

  const res = await sdk.catalog.getPricesInOneCountry('uk')
  assert.deepEqual(res, PriceData)
})

test('Get prices for one service', async ({ assert }) => {
  const PriceData: CountryPriceType[] = [
    {
      id: 'uk',
      price: 210,
      count: 143
    }
  ]

  const res = await sdk.catalog.getPricesForOneService(12)
  assert.deepEqual(res, PriceData)
})

test('Get prices for one service in one country', async ({ assert }) => {
  const PriceData: PricesCount = {
    price: 210,
    count: 143
  }

  const res = await sdk.catalog.getPricesForServiceInOneCountry(12, 'uk')
  assert.deepEqual(res, PriceData)
})
