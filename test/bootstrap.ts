process.env.NODE_ENV = 'test'

import { processCliArgs, configure, run } from '@japa/runner'
import { assert } from '@japa/assert'
import { specReporter } from '@japa/spec-reporter'
import { runFailedTests } from '@japa/run-failed-tests'

import OpenApiMocker from 'open-api-mocker'

const openApiMocker = new OpenApiMocker({
  port: 3000,
  schema: 'test/dist.yaml'
})

configure({
  ...processCliArgs(process.argv.slice(2)),
  ...{
    files: ['test/**/*.spec.ts'],
    plugins: [assert(), runFailedTests()],
    reporters: [specReporter()],
    importer: (filePath: string) => import(filePath),
    forceExit: true
  }
})
;(async () => {
  await openApiMocker.validate()
  let sigintReceived = false

  process.on('SIGINT', async () => {
    if (sigintReceived) process.exit(0)

    sigintReceived = true

    console.log('SIGINT received. Shutting down...')
    console.log('Press ^C again to force stop.')

    await openApiMocker.shutdown()
    process.exit(0)
  })
  await openApiMocker.mock()
})().then(() => {
  run()
})
